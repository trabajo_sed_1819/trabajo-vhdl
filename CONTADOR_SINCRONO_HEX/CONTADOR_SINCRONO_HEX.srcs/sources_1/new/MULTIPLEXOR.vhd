----------------------------------------------------------------------------------
-- Nombre: TOP
-- Descripci�n: Estructura final, formada por el m�dulo de tratamiento, varios
--               modificadores de frecuencia, un contador y un m�dulo de salida.
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
----------------------------------------------------------------------------------

entity MULTIPLEXOR is
        GENERIC(
            N_BITS: integer :=4
        );
        PORT(
            a:   in STD_LOGIC_VECTOR(N_BITS-1 downto 0);
            b:   in STD_LOGIC_VECTOR(N_BITS-1 downto 0);
            sw:  in STD_LOGIC;
            res: out STD_LOGIC_VECTOR(N_BITS-1 downto 0)   
        );   
end MULTIPLEXOR;

architecture Behavioral of MULTIPLEXOR is
    signal s_out: STD_LOGIC_VECTOR(N_BITS downto 0);
begin
    process(sw)
    begin
        if(sw='0') then
            res <= a;
        else
            res <= b;        
        end if;
    end process;
end Behavioral;
