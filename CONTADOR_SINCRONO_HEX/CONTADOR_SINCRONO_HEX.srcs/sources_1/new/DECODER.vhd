----------------------------------------------------------------------------------
-- Nombre: TOP
-- Descripci�n: Estructura final, formada por el m�dulo de tratamiento, varios
--               modificadores de frecuencia, un contador y un m�dulo de salida.
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
----------------------------------------------------------------------------------

entity DECODER is
    Port (
            code : in STD_LOGIC_VECTOR (3 downto 0);
            led : out STD_LOGIC_VECTOR (6 downto 0)
            );
end DECODER;

architecture dataflow of DECODER is

begin
    WITH code SELECT
        led <=  "0000001" WHEN "0000",--0 : 01 hex
                "1001111" WHEN "0001",--1 : 4f hex
                "0010010" WHEN "0010",--2 : 12 hex
                "0000110" WHEN "0011",--3 : 06 hex
                "1001100" WHEN "0100",--4 : 4c hex
                "0100100" WHEN "0101",--5 : 24 hex
                "0100000" WHEN "0110",--6 : 20 hex
                "0001111" WHEN "0111",--7 : 0f hex
                "0000000" WHEN "1000",--8 : 00 hex
                "0000100" WHEN "1001",--9 : 04 hex
                "0000010" WHEN "1010",--a : 02 hex
                "1100000" WHEN "1011",--b : 60 hex
                "1110010" WHEN "1100",--c : 72 hex
                "1000010" WHEN "1101",--d : 42 hex
                "0010000" WHEN "1110",--e : 10 hex
                "0111000" WHEN "1111",--F : 38 hex  
                "1111110" WHEN others;  --: 7e hex

end dataflow;
