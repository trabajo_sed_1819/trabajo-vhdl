----------------------------------------------------------------------------------
-- Nombre: CONTADOR_NBITS
-- Descripci�n: Cuenta nbits hasta el limite establecido y despu�s vuelve a cero. 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
----------------------------------------------------------------------------------

entity CONTADOR_NBITS is
    generic(
        n_bits: positive := 4        
    );
    Port ( 
           in_carry :   in STD_LOGIC;
           out_carry :  out STD_LOGIC;
           clk :        in STD_LOGIC;
           rst :        in STD_LOGIC;
           res :        out STD_LOGIC_VECTOR (n_bits-1 downto 0);
           en :         in STD_LOGIC
     );
     
end CONTADOR_NBITS;

----------------------------------------------------------------------------------

architecture Behavioral of CONTADOR_NBITS is
    signal cnt: unsigned (n_bits-1 downto 0):=(others=>'0');
    signal lim: positive := 2**n_bits-1; --15 para hex, 9 para BCD 
     --signal lim: positive := 2; --15 para hex, 9 para BCD 
begin

add:process(clk,rst)
    --variable cnt: unsigned (n_bits downto 0);
    --variable lim: positive := 2**n_bits;
    
    begin
        if rst='1' then
            cnt <= (others => '0');            
        elsif rising_edge(clk) then
            if en='1' and in_carry='1' then
            
                --en funcionamiento normal--
                if cnt < lim then
               
                     cnt <= cnt + 1;                                                            
                else
                     cnt <= (others => '0'); 
                end if;
                --fin del funcionamiento normal-- 
                
            end if;
        end if;
        
    --out_carry <= cnt(n_bits);
    res <= std_logic_vector( cnt(n_bits-1 downto 0) );     
end process;
       
carry:process(cnt,in_carry)
    begin
        out_carry <='0';
        if cnt = lim and in_carry='1' then
            out_carry<='1';
        end if;        
end process;          
    
end Behavioral;

