----------------------------------------------------------------------------------
-- Nombre: TOP
-- Descripci�n: Estructura final, formada por el m�dulo de tratamiento, varios
--               modificadores de frecuencia, un contador y un m�dulo de salida.
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
----------------------------------------------------------------------------------

entity TOP is
    Port (
        RESET:       in STD_LOGIC;                      --reset
        CLOCK:       in STD_LOGIC;                      --reloj
        PAUSA_SW:    in STD_LOGIC;                      --pausa la cuenta
        DISPLAY_NUM: out STD_LOGIC_VECTOR (6 downto 0); --el c�digo de los segmentos
        DISPLAY_SEL: out STD_LOGIC_VECTOR (7 downto 0); --necesario para que el resto de displays est�n apagados
        LEDS:        out STD_LOGIC_VECTOR (1 downto 0)  --leds que siguen el estado de reset y pausa
    );
end TOP;

----------------------------------------------------------------------------------

architecture STRUCTURAL of TOP is

--clk_divider crea m�ltiplos y subm�ltiplos de la frecuencia de reloj

    COMPONENT clk_divider
        generic (
            f_in: in positive:=2; --frecuencia de entrada
            f_out: in positive:=1 --frecuencia de salida
        );
        PORT(
            clk_in :  in STD_LOGIC;
            rst :     in STD_LOGIC;
            clk_out : out STD_LOGIC
        );
    END COMPONENT;
 
 
--ttmto_entradas sincroniza y adapta (s_out) las se�ales de entrada (s_in)

    COMPONENT ttmto_entradas
        PORT(
            rst:   in STD_LOGIC;
            clk:   in STD_LOGIC;
            s_in:  in STD_LOGIC;
            s_out: out STD_LOGIC
        );
        
    END COMPONENT;
    
    
--contador_total se encarga de contar con cada flanco de reloj

    COMPONENT contador_total
         PORT(
               rst:    in STD_LOGIC;
               clk:    in STD_LOGIC;
               pausa:  in STD_LOGIC;
               uds:    out STD_LOGIC_VECTOR (3 downto 0);
               dec:    out STD_LOGIC_VECTOR (3 downto 0)
           );
    
    
    END COMPONENT;


--modulo_salida adapta el resultado del contador para mostrarlo en los displays  

    COMPONENT mod_salida
         PORT(
               clk:         in STD_LOGIC;
               uds:         in STD_LOGIC_VECTOR (3 downto 0);
               dec:         in STD_LOGIC_VECTOR (3 downto 0);
               disp_num:    out STD_LOGIC_VECTOR (6 downto 0);
               disp_sel:    out STD_LOGIC_VECTOR (1 downto 0)
           );
    
    
    END COMPONENT;

----------------------------------------------------------------------------------
--SE�ALES INTERNAS. Las correpsondientes a los puertos se conectan directamente al puerto

    signal CLK_1kHz:    STD_LOGIC;
    signal CLK_170Hz:  STD_LOGIC;
    signal CLK_1Hz:     STD_LOGIC;
    
    signal PAUSA_SINC:  STD_LOGIC;
    signal DISPLAY_inter: STD_LOGIC_VECTOR(1 downto 0);    
    signal UDS:         STD_LOGIC_VECTOR (3 downto 0);
    signal DEC:         STD_LOGIC_VECTOR (3 downto 0);
    
----------------------------------------------------------------------------------    

begin
DISPLAY_SEL<= "111111" & DISPLAY_inter(1 downto 0); 
LEDS <= PAUSA_SW & RESET; 


clk1: clk_divider
       GENERIC MAP(
           f_in  => 200000000, 
           f_out => 1000  
       ) 
       PORT MAP (
         clk_in => CLOCK,
         rst => RESET,
         clk_out=> CLK_1kHz
       );
 
clk2: clk_divider
       GENERIC MAP(
           f_in  => 1000, 
           f_out => 170  
       ) 
       PORT MAP (
         clk_in => CLK_1kHz,
         rst => RESET,
         clk_out=> CLK_170Hz
       );
clk3: clk_divider
       GENERIC MAP(
           f_in  => 170,
           f_out => 1  
       ) 
       PORT MAP (
         clk_in => CLK_170Hz,
         rst => RESET,
         clk_out=> CLK_1Hz
       );  
----------------------------------------------------------------------------------

ttmto: ttmto_entradas
        PORT MAP(
            rst   => RESET,
            clk   => CLK_1kHz,
            s_in  => PAUSA_SW,
            s_out => PAUSA_SINC
        );  
 
----------------------------------------------------------------------------------

cnt_tot: contador_total
        PORT MAP(
            rst   => RESET,
            clk   => CLK_1Hz,
            pausa => PAUSA_SINC,
            uds   => UDS,  
            dec   => DEC   
        );   
        
----------------------------------------------------------------------------------
              
mod_sal: mod_salida
        PORT MAP(
            clk => CLK_170Hz,
            uds => UDS,
            dec => DEC,
            disp_num => DISPLAY_NUM,
            disp_sel => DISPLAY_inter
        );
        
             
end STRUCTURAL;