----------------------------------------------------------------------------------
-- Nombre: CLK_DIVIDER
-- Descripción: Transforma la frecuencia de reloj en una menor
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
----------------------------------------------------------------------------------
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

entity CLK_DIVIDER is
    generic (
            f_in:  in positive:=2; --frecuencia de entrada
            f_out: in positive:=1 --frecuencia de salida
    );
    Port ( 
            clk_in :    in STD_LOGIC;          
            rst :       in STD_LOGIC;
            clk_out :   out STD_LOGIC
    );
end CLK_DIVIDER;

----------------------------------------------------------------------------------

architecture Behavioral of clk_divider is
signal clk_sig: std_logic;
constant frec: integer :=f_in/(f_out*2);
begin

  process (clk_in,rst)
  variable cnt:integer;
  begin
		if (rst='1') then
		  cnt:=1;
		  clk_sig<='0';
		elsif rising_edge(clk_in) then
			if (cnt>=frec) then
				cnt:=1;
				clk_sig<=not(clk_sig);
			else
				cnt:=cnt+1;
			end if;
		end if;
  end process;
  clk_out<=clk_sig;
end Behavioral;
