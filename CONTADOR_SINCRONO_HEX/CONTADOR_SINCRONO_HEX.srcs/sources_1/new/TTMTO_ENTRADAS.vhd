----------------------------------------------------------------------------------
-- Nombre: TTMTO_ENTRADAS 
-- Descripci�n: En esta versi�n cumple �nicamente la funci�n de sincronizador, pero
--              est� preparado para poder albergar cualquier tratamiento deseado a
--              las se�ales de los pines (p.ej, evitar efecto rebote)
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
----------------------------------------------------------------------------------
-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity TTMTO_ENTRADAS is
    Port (
           rst :    in STD_LOGIC;
           clk :    in STD_LOGIC;
           s_in :   in STD_LOGIC;
           s_out :  out STD_LOGIC
    );
end TTMTO_ENTRADAS;

----------------------------------------------------------------------------------

architecture Behavioral of TTMTO_ENTRADAS is
begin

p1:process(clk,rst)
    begin
    
    if rst='1' then
        s_out <='0';
    elsif rising_edge(clk) then
        s_out <=s_in;
    end if;
end process;    
end Behavioral;
