----------------------------------------------------------------------------------
-- Nombre: CONTADOR_TOTAL
-- Descripci�n: Lleva una cuenta con un reloj de 1 Hz. PAUSA equivale a la se�al enable
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
----------------------------------------------------------------------------------

entity CONTADOR_TOTAL is
    Port (
           rst : in STD_LOGIC;
           clk : in STD_LOGIC;
           pausa : in STD_LOGIC;
           uds : out STD_LOGIC_VECTOR (3 downto 0);
           dec : out STD_LOGIC_VECTOR (3 downto 0)
    );
end CONTADOR_TOTAL;

----------------------------------------------------------------------------------

architecture structural of CONTADOR_TOTAL is
    COMPONENT contador_nbits
        GENERIC(
            n_bits: positive :=4
        );
        PORT(
            in_carry:   in STD_LOGIC;
            out_carry:  out STD_LOGIC;
            clk:        in STD_LOGIC;
            rst:        in STD_LOGIC;
            en:         in STD_LOGIC;
            res:        out STD_LOGIC_VECTOR(3 downto 0)
        );
    END COMPONENT;
    
    signal CARRY: STD_LOGIC;
    signal PLAY: STD_LOGIC;


----------------------------------------------------------------------------------
begin
    PLAY <=not pausa;
    
    cnt_uds: contador_nbits
    PORT MAP(
        in_carry    => '1',
        out_carry   => CARRY,
        rst         => rst,
        clk         => clk,
        en          => PLAY,
        res         => uds
    );

    ----------------------------------------------------------------------------------
    cnt_dec: contador_nbits
    PORT MAP(
        in_carry    => CARRY,
        --out_carry
        rst         => rst,
        clk         => clk,
        en          => PLAY,
        res         => dec
    );
end structural;
