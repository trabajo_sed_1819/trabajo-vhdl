----------------------------------------------------------------------------------
-- Nombre: TOP
-- Descripci�n: Estructura final, formada por el m�dulo de tratamiento, varios
--               modificadores de frecuencia, un contador y un m�dulo de salida.
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
----------------------------------------------------------------------------------

entity MOD_SALIDA is
    PORT(
        clk:         in STD_LOGIC;
        uds:         in STD_LOGIC_VECTOR (3 downto 0);
        dec:         in STD_LOGIC_VECTOR (3 downto 0);
        disp_num:    out STD_LOGIC_VECTOR (6 downto 0);
        disp_sel:    out STD_LOGIC_VECTOR (1 downto 0)
  );

end MOD_SALIDA;

----------------------------------------------------------------------------------

architecture Behavioral of MOD_SALIDA is

    COMPONENT multiplexor is
        GENERIC(
            N_BITS: integer :=4
        );
        PORT(
            a:   in STD_LOGIC_VECTOR(N_BITS-1 downto 0);
            b:   in STD_LOGIC_VECTOR(N_BITS-1 downto 0);
            sw:  in STD_LOGIC;
            res: out STD_LOGIC_VECTOR(N_BITS-1 downto 0)   
        );    
    END COMPONENT;
    
----------------------------------------------------------------------------------

    COMPONENT decoder is
        PORT(
            code:  in STD_LOGIC_VECTOR(3 downto 0);
            led:   out STD_LOGIC_VECTOR(6 downto 0)
        );
        
    END COMPONENT;
    
----------------------------------------------------------------------------------
    signal MUX_OUT: STD_LOGIC_VECTOR(3 downto 0);   
begin
disp_sel <= ((NOT clk) & (clk) );

mux: multiplexor
        GENERIC MAP(
            N_BITS => 4
        )
        PORT MAP(
            a   => uds,
            b   => dec,
            sw  => clk,
            res => MUX_OUT
        );
        
decod: decoder
        PORT MAP(
            code => MUX_OUT,
            led  => disp_num
        );        
end Behavioral;
