library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.ALL;
-------------------------------------
entity TB is
end TB;
-------------------------------------
architecture Behavioral of TB is
    COMPONENT TOP
     PORT(
           RESET:       in STD_LOGIC;
           CLOCK:       in STD_LOGIC;
           PAUSA_SW:    in STD_LOGIC;
           DISPLAY_NUM: out STD_LOGIC_VECTOR (6 downto 0);
           DISPLAY_SEL: out STD_LOGIC_VECTOR (1 downto 0)
       );


    END COMPONENT;
    

    signal clk : std_logic := '0';
    signal rst : std_logic := '0';
    signal PAUSA : std_logic :='0';
    signal display_num : STD_LOGIC_VECTOR(6 downto 0);
    signal display_sel : STD_LoGIC_VECTOR(1 DOWNTO 0);



    constant clk_period : time := 2 ns;


begin
    -- Instantiate the Unit Under Test (UUT)
    
    top_u: top
        PORT MAP(          
            clock         => clk,
            reset         => rst,
            pausa_sw       => PAUSA,
            display_num         => display_num,
            display_sel        => display_sel
        );



    -- Clock process definitions

    clk_p :process
    begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
    end process;
 
    -- test de respuesta
    
   
    test: process
    begin		
        -- hold reset state for 100 ns.
        rst<='0';
		wait for 20 ns;		
		rst<='1';
		wait for 30 ns;		
		rst<='0';
        wait for 10000 ns;
        pausa <='1';
        wait for 50 ns;
        pausa <='0';

		wait;	
    end process;

end Behavioral;

